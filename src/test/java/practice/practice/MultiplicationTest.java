package practice.practice;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import practice.Calculator;

public class MultiplicationTest  {
	
	@Test
	public void testMultiplication1() {
		Calculator calc = new Calculator();
		int output = calc.multiply(3, 2);
		assertEquals(6,output);

	}
	
	@Test
	public void testMultiplication2() {
		Calculator calc = new Calculator();
		int output = calc.multiply(1, 1);
		assertEquals(1,output);

	}
}
