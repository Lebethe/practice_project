package practice.practice;



import practice.Calculator;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class AddTest  {
	
	@Test
	public void testAdd() {
		
		Calculator calc = new Calculator();
		int output = calc.add(1, 2);
		assertEquals(3,output);
		
	}



}
