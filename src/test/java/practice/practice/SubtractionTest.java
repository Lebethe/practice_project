package practice.practice;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import practice.Calculator;

public class SubtractionTest {
	
	@Test
	public void testSubtraction() {
		Calculator calc = new Calculator();
		int output = calc.subtract(3, 2);
		assertEquals(1,output);
	}

}
